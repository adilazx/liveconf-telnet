"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LiveCpe_1 = require("./lib/models/LiveCpe");
var FetchQueue_1 = require("./lib/FetchQueue");
var confgen_1 = require("./confgen");
var logger_1 = require("./logger");
var config = require('../fetcher.json');
var logger = logger_1.appLogger.child({ mod: 'FetchQueue' });
function fetchCpes(count) {
    return new Promise(function (resolve, reject) {
        LiveCpe_1.LiveCpeModel
            .find()
            .where('isActive')
            .equals(true)
            .sort({ lastPoll: -1 })
            .limit(count)
            .exec(function (err, docs) {
            if (err)
                reject(err);
            else
                resolve(docs);
        });
    });
}
function convertToQueueEntry(cpes) {
    return cpes
        .filter(function (cpe) {
        try {
            confgen_1.getEqTypeFromString(cpe.eqType);
            if (!cpe.ipAddress)
                return false;
            return true;
        }
        catch (err) {
            return false;
        }
    })
        .map(function (cpe) {
        var cpeType = confgen_1.getEqTypeFromString(cpe.eqType);
        return {
            cpeId: cpe._id,
            cpeType: cpeType,
            host: cpe.ipAddress
        };
    });
}
function updateCpeLastPoll(_id) {
    return new Promise(function (resolve, reject) {
        LiveCpe_1.LiveCpeModel.update({
            _id: _id,
        }, {
            $set: {
                lastPoll: new Date()
            }
        }, {
            multi: false
        }, function (err, data) {
            if (err)
                reject(err);
            else
                resolve(data);
        });
    });
}
var options = {
    maxConcurrentJobs: 2,
    startHour: 0,
    endHour: 23,
    logger: logger,
    onJobStart: function (job) {
        updateCpeLastPoll(job.cpeId).catch(function (err) { return logger.warn(err); });
    },
    onJobDone: function (job, err, config) {
        fetchCpes(1)
            .then(function (cpes) {
            queue.addJobs.apply(queue, convertToQueueEntry(cpes));
        });
        console.log(job.cpeId, err ? err.message : null);
    },
    onQueueDone: function () {
    }
};
options = Object.assign(options, config);
var queue = new FetchQueue_1.FetchQueue(options);
function startFetch() {
    fetchCpes(options.maxConcurrentJobs)
        .then(function (cpes) {
        queue.addJobs.apply(queue, convertToQueueEntry(cpes));
        queue.start();
    })
        .catch(function (err) { return logger.error(err); });
}
exports.startFetch = startFetch;
//# sourceMappingURL=fetcher.js.map