"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Mongoose = require("mongoose");
exports.ConfigSchema = new Mongoose.Schema({
    csc: { type: String, required: true, index: { unique: false } },
    clientName: { type: String, required: false },
    eqType: { type: String, required: true },
    eqName: { type: String, required: true },
    wo: { type: String, required: false },
    content: { type: String, required: true },
    isActive: { type: Boolean, default: true },
    isExported: { type: Boolean },
    fileName: { type: String },
    mac: { type: String },
    version: { type: Number, default: 1, index: { unique: false } },
    created: { type: Date },
    createdBy: { type: String },
    exported: { type: Date },
    exportedBy: { type: String },
    downloaded: { type: Date },
    dlMethod: { type: String },
    dlLink: { type: String }
});
exports.ConfigModel = Mongoose.model('Config', exports.ConfigSchema);
//# sourceMappingURL=Config.js.map