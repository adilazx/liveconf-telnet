"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Mongoose = require("mongoose");
exports.LiveCpeSchema = new Mongoose.Schema({
    csc: { type: String, required: true, index: { unique: false } },
    clientName: { type: String, required: false },
    eqType: { type: String, required: true },
    eqName: { type: String, required: true },
    wo: { type: String, required: false },
    isActive: { type: Boolean, default: true },
    isCeased: { type: Boolean, default: false },
    ipAddress: { type: String, required: true },
    created: { type: Date, default: Date.now },
    modified: { type: Date },
    failures: { type: Number, default: 0 },
    lastError: { type: String },
    lastPoll: { type: Date },
    lastFetch: { type: Date },
    nextPoll: { type: Date },
    configsCount: { type: Number, default: 0 },
    pollsCount: { type: Number, default: 0 },
    hash: { type: String },
    configs: [{ output: String, created: Date }],
});
exports.LiveCpeModel = Mongoose.model('LiveCpe', exports.LiveCpeSchema);
//# sourceMappingURL=LiveCpe.js.map