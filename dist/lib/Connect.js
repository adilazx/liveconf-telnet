"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Teletype_1 = require("./Teletype");
exports.connect = function (host, options) {
    if (typeof options !== 'object' || !options)
        throw new TypeError("Invalid options");
    if (typeof host !== 'string')
        throw new TypeError('Host must be a string.');
    if (!(options.loginPrompt instanceof RegExp))
        throw new TypeError('Invalid login prompt.');
    if (!(options.passwordPrompt instanceof RegExp))
        throw new TypeError('Invalid password prompt.');
    if (!(options.shellPrompt instanceof RegExp))
        throw new TypeError('Invalid shell prompt.');
    if (!(options.failedLoginMatch instanceof RegExp))
        throw new TypeError('Invalid failed login match.');
    if (!Array.isArray(options.credentials) || options.credentials.length === 0)
        throw new TypeError('Invalid creadentials.');
    var credentials = options.credentials, lineBreak = options.lineBreak, loginPrompt = options.loginPrompt, passwordPrompt = options.passwordPrompt, shellPrompt = options.shellPrompt, failedLoginMatch = options.failedLoginMatch, port = options.port, timeout = options.timeout, sendOnConnect = options.sendOnConnect;
    var maxAttempts = options.credentials.length;
    var logger = options.logger ? options.logger.child({ host: host }) : null;
    var teletype;
    var attemptConnect = function (attempt) {
        return new Promise(function (resolve, reject) {
            teletype = new Teletype_1.Teletype(host, {
                logger: logger,
                port: port,
                timeout: timeout
            });
            var username = credentials[attempt][0];
            var password = credentials[attempt][1];
            if (sendOnConnect)
                teletype.exec(sendOnConnect, null);
            teletype
                .readUntil(loginPrompt, lineBreak)
                .then(function (res) {
                logger && logger.debug("Sending username");
                return teletype.exec(username, passwordPrompt, lineBreak);
            })
                .then(function (res) {
                logger && logger.debug("Sending password");
                return teletype.exec(password, [shellPrompt, failedLoginMatch], lineBreak);
            })
                .then(function (res) {
                var prompt = res[0];
                if (failedLoginMatch.test(prompt)) {
                    logger && logger.info("Authentication failed");
                    teletype.close();
                    return reject(new Error('Authentication failed'));
                }
                ;
                logger && logger.info("Authenticated, prompt is [" + prompt + "]");
                resolve({
                    teletype: teletype,
                    prompt: prompt,
                    credentials: [username, password]
                });
            })
                .catch(function (err) {
                logger && logger.warn(err.message);
                teletype.close();
                reject(err);
            });
        });
    };
    return new Promise(function (resolve, reject) {
        logger && logger.info("Connecting to " + host);
        var executeAttempt = function (attempt) {
            if (attempt === void 0) { attempt = 0; }
            logger && logger.debug("Connection attempt #" + attempt);
            attemptConnect(attempt)
                .then(function (res) { return resolve(res); })
                .catch(function (err) {
                if (attempt === maxAttempts - 1)
                    reject(new Error('All connection attempts have failed.'));
                else
                    executeAttempt(attempt + 1);
            });
        };
        executeAttempt();
    });
};
exports.enable = function (connection, options) {
    var enablePrompt = options.enablePrompt, enableShellPrompt = options.enableShellPrompt, enablePassword = options.enablePassword, logger = options.logger;
    return new Promise(function (resolve, reject) {
        if (!enablePassword || typeof enablePassword !== 'string')
            return reject(new TypeError('invalid enable password'));
        logger && logger.debug('Enabling....');
        connection.teletype.exec('enable', enablePrompt)
            .then(function (data) { return connection.teletype.exec(enablePassword, enableShellPrompt); })
            .then(function (data) {
            var prompt = data[0];
            connection.prompt = prompt;
            logger && logger.debug("Enabled, prompt is now " + prompt);
            resolve(prompt);
        })
            .catch(function (err) {
            logger && logger.debug("Enable failed");
            reject(err);
        });
    });
};
//# sourceMappingURL=Connect.js.map