"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _1 = require("./");
var logger_1 = require("../../logger");
var credentials = require('../../../credentials.json');
var type = 'M800';
var defaultOptions = {
    loginPrompt: /username:/i,
    passwordPrompt: /password:/i,
    shellPrompt: /([a-zA-Z0-9_-]{1,})(\#|\>)/,
    failedLoginMatch: /denied/i,
    credentials: credentials[type],
    timeout: 5000,
    logger: logger_1.appLogger.child({ type: type })
};
var fetcher = function (host, options) {
    if (options === void 0) { options = {}; }
    return new Promise(function (resolve, reject) {
        _1.connect(host, Object.assign(defaultOptions, options))
            .then(function (connection) {
            connection
                .teletype.exec('show running-config', /\w/)
                .then(function (data) {
                return connection.teletype.readUntilEnd(new RegExp(connection.prompt), /--MORE--/i, '   ');
            })
                .then(function (data) {
                connection.teletype.exec('exit', null);
                connection.teletype.close();
                resolve(data[1]);
            })
                .catch(function (err) {
                connection.teletype.close();
                reject(err);
            });
        })
            .catch(function (err) {
            reject(err);
        });
    });
};
exports.default = fetcher;
//# sourceMappingURL=M800.js.map