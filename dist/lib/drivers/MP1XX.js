"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _1 = require("./");
var logger_1 = require("../../logger");
var credentials = require('../../../credentials.json');
var type = 'MP1XX';
var defaultOptions = {
    loginPrompt: /login:/i,
    passwordPrompt: /password:/i,
    shellPrompt: /\/>/,
    failedLoginMatch: /denied/i,
    credentials: credentials[type],
    timeout: 5000,
    logger: logger_1.appLogger.child({ type: type })
};
var fetcher = function (host, options) {
    if (options === void 0) { options = {}; }
    return new Promise(function (resolve, reject) {
        _1.connect(host, Object.assign(defaultOptions, options))
            .then(function (connection) {
            connection
                .teletype.exec('sh log stop', /\/>/)
                .then(function (data) { return connection.teletype.exec('conf', /\/CONFiguration>/i); })
                .then(function (data) {
                connection.teletype.exec('CF VIEW', /\*/);
                return connection.teletype.readUntilEnd(/CopyConfigurationFile/i, /-- More --/i, '\r\n');
            })
                .then(function (data) {
                connection.teletype.exec('exit', null);
                connection.teletype.close();
                resolve(data[1]);
            })
                .catch(function (err) {
                reject(err);
                connection.teletype.close();
            });
        })
            .catch(function (err) {
            reject(err);
        });
    });
};
exports.default = fetcher;
//# sourceMappingURL=MP1XX.js.map