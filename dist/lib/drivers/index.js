"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Connect_1 = require("../Connect");
exports.connect = Connect_1.connect;
exports.enable = Connect_1.enable;
var M800_1 = require("./M800");
var MP1XX_1 = require("./MP1XX");
var CISCO_GENERIC_1 = require("./CISCO_GENERIC");
var RAISECOM_GENERIC_1 = require("./RAISECOM_GENERIC");
var HUAWEI_GENERIC_1 = require("./HUAWEI_GENERIC");
exports.drivers = new Map();
exports.drivers.set('M800', { fetchConfig: M800_1.default });
exports.drivers.set('MP1XX', { fetchConfig: MP1XX_1.default });
exports.drivers.set('CISCO_GENERIC', { fetchConfig: CISCO_GENERIC_1.default });
exports.drivers.set('RAISECOM_GENERIC', { fetchConfig: RAISECOM_GENERIC_1.default });
exports.drivers.set('HUAWEI_GENERIC', { fetchConfig: HUAWEI_GENERIC_1.default });
//# sourceMappingURL=index.js.map