"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _1 = require("./");
var logger_1 = require("../../logger");
var credentials = require('../../../credentials.json');
var type = 'RAISECOM_GENERIC';
var defaultOptions = {
    loginPrompt: /login:/i,
    passwordPrompt: /password:/i,
    shellPrompt: /([a-zA-Z0-9_-]{1,})(\#|\>)/,
    failedLoginMatch: /login/i,
    credentials: credentials[type],
    sendOnConnect: '\r',
    timeout: 5000,
    logger: logger_1.appLogger.child({ type: type })
};
var fetcher = function (host, options) {
    if (options === void 0) { options = {}; }
    return new Promise(function (resolve, reject) {
        _1.connect(host, Object.assign(defaultOptions, options))
            .then(function (connection) {
            _1.enable(connection, {
                enablePrompt: options.passwordPrompt,
                enableShellPrompt: new RegExp(connection.prompt.substr(0, connection.prompt.length - 2)),
                logger: options.logger,
                enablePassword: options.enablePassword
            })
                .then(function (data) { return connection.teletype.exec('show running-config', /system/); })
                .then(function (data) {
                return connection.teletype.readUntilEnd(new RegExp(connection.prompt), /--More--/i, ' ');
            })
                .then(function (data) {
                connection.teletype.exec('exit', null);
                connection.teletype.close();
                resolve(data[1]);
            })
                .catch(function (err) {
                connection.teletype.close();
                reject(err);
            });
        })
            .catch(function (err) {
            reject(err);
        });
    });
};
exports.default = fetcher;
//# sourceMappingURL=RAISECOM_GENERIC.js.map