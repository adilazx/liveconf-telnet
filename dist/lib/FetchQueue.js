"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var drivers_1 = require("./drivers");
var options = require('../../options.json');
var FetchQueue = (function () {
    function FetchQueue(options) {
        var _this = this;
        this.queue = [];
        this._running = false;
        this._processingQueue = false;
        this.onTimer = function () {
            var d = new Date();
            var hours = d.getHours();
            if (_this.startHour > _this.endHour && (hours >= _this.startHour || hours < _this.endHour))
                _this.startProcessingQueue();
            else if (hours >= _this.startHour && hours < _this.endHour)
                _this.startProcessingQueue();
            else
                _this.stopProcessingQueue();
        };
        this.maxConcurrentJobs = options.maxConcurrentJobs;
        this.logger = options.logger;
        this.onJobStart = options.onJobStart;
        this.onJobDone = options.onJobDone;
        this.onQueueDone = options.onQueueDone;
        this.startHour = options.startHour;
        this.endHour = options.endHour;
    }
    FetchQueue.prototype.addJobs = function () {
        var entries = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            entries[_i] = arguments[_i];
        }
        (_a = this.queue).push.apply(_a, entries);
        this.logger.debug("Added " + entries.length + " jobs");
        var _a;
    };
    Object.defineProperty(FetchQueue.prototype, "size", {
        get: function () {
            return this.queue.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FetchQueue.prototype, "running", {
        get: function () {
            return this._running;
        },
        enumerable: true,
        configurable: true
    });
    FetchQueue.prototype.start = function () {
        if (!this._running) {
            this.timer = setInterval(this.onTimer, 5000);
            this._running = true;
            this.logger.info('Starting');
        }
    };
    FetchQueue.prototype.stop = function () {
        if (this._running) {
            clearInterval(this.timer);
            this._running = false;
            this.logger.info('Stopped');
        }
    };
    FetchQueue.prototype.startProcessingQueue = function () {
        if (this._processingQueue)
            return;
        this.logger.info('Start processing queue');
        this._processingQueue = true;
        this.executeJobs();
    };
    FetchQueue.prototype.stopProcessingQueue = function () {
        if (!this._processingQueue)
            return;
        this.logger.info('Stop processing queue');
        this._processingQueue = false;
    };
    FetchQueue.prototype.onJobCompleted = function (job, err, config) {
        this.logger.debug("Job completed [" + job.cpeId + "]");
        var jobIndex = this.queue.indexOf(job);
        this.queue.splice(jobIndex, 1);
        this.onJobDone(job, err, config);
        this.executeJobs();
    };
    Object.defineProperty(FetchQueue.prototype, "activeCount", {
        get: function () {
            return this.queue.filter(function (entry) { return entry.active === true; }).length;
        },
        enumerable: true,
        configurable: true
    });
    FetchQueue.prototype.executeJobs = function () {
        var _this = this;
        if (this.size > 0) {
            var activeJobsCount_1 = this.activeCount;
            activeJobsCount_1 < this.maxConcurrentJobs && this.logger.info("Executing jobs, active jobs: " + activeJobsCount_1 + ", queue size: " + this.size);
            this.queue.forEach(function (job) {
                if (!job.active && activeJobsCount_1 < _this.maxConcurrentJobs) {
                    _this.executeJob(job);
                }
            });
        }
        else {
            this.logger.warn('Queue is empty');
            this.onQueueDone();
        }
    };
    FetchQueue.prototype.executeJob = function (job) {
        var _this = this;
        job.active = true;
        this.logger.info("Running job [" + job.cpeId + "]");
        this.onJobStart(job);
        drivers_1.drivers.get(job.cpeType).fetchConfig(job.host, options[job.cpeType])
            .then(function (config) {
            _this.onJobCompleted(job, null, config);
        })
            .catch(function (err) { return _this.onJobCompleted(job, err, null); });
    };
    return FetchQueue;
}());
exports.FetchQueue = FetchQueue;
//# sourceMappingURL=FetchQueue.js.map