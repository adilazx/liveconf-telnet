"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var net = require("net");
exports.TELNET_EOL = '\r\n';
var Teletype = (function () {
    function Teletype(host, options) {
        if (options === void 0) { options = {}; }
        this._host = host;
        this._port = options.port || 23;
        this._timeout = options.timeout || options.timeout === false ? options.timeout : false;
        this._logger = options.logger;
        if (typeof host !== 'string') {
            throw new TypeError('Host must be a string.');
        }
        if (typeof this._port !== 'number') {
            throw new TypeError('port must be a number.');
        }
        if (typeof this._timeout !== 'number' && this._timeout !== false) {
            throw new TypeError('options.timeout must be a number or false.');
        }
    }
    Object.defineProperty(Teletype.prototype, "logger", {
        get: function () {
            return this._logger;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Teletype.prototype, "host", {
        get: function () {
            return this._host;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Teletype.prototype, "port", {
        get: function () {
            return this._port;
        },
        enumerable: true,
        configurable: true
    });
    Teletype.prototype._lazyConnect = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var timeout;
            if (_this._client && !_this._client.connecting) {
                return resolve(_this._client);
            }
            if (!_this._client) {
                _this._logger && _this._logger.debug('Connecting...');
                _this._client = net.connect({
                    host: _this._host,
                    port: _this._port
                });
                if (_this._timeout) {
                    timeout = setTimeout(function () {
                        _this._logger && _this._logger.debug('Connection timeout');
                        _this._client.destroy();
                        reject(new Error('Connect timeout'));
                    }, _this._timeout);
                }
                _this._client.setEncoding('ascii');
                _this._client.once('error', function (err) {
                    _this._logger && _this._logger.error(err);
                    if (timeout)
                        clearTimeout(timeout);
                    reject(err);
                });
                _this._client.once('connect', function () {
                    _this._logger && _this._logger.debug('Connected');
                    _this._client.removeListener('error', reject);
                    if (timeout)
                        clearTimeout(timeout);
                    resolve(_this._client);
                });
                _this._client.once('end', function () {
                    _this._logger && _this._logger.debug('Socket disconnected by remote end');
                });
                _this._client.once('close', function () {
                    _this._logger && _this._logger.debug('Socket closed');
                });
            }
        });
    };
    Teletype.prototype.exec = function (command, match, lineBreak) {
        var _this = this;
        if (lineBreak === void 0) { lineBreak = exports.TELNET_EOL; }
        if (typeof command !== 'string') {
            return Promise.reject(new TypeError('Command must be a string.'));
        }
        this._logger && this._logger.debug("Executing [" + command + "]");
        return this._lazyConnect().then(function (client) {
            var promise;
            if (match) {
                promise = _this.readUntil(match, lineBreak);
            }
            client.write(command + exports.TELNET_EOL);
            if (match)
                return promise;
        });
    };
    Teletype.prototype.readUntil = function (match, lineBreak) {
        var _this = this;
        if (lineBreak === void 0) { lineBreak = exports.TELNET_EOL; }
        if (!(match instanceof RegExp) && !Array.isArray(match)) {
            return Promise.reject(new TypeError('Match must be a RegExp or an array of RegExp.'));
        }
        var isMatch = function (s) {
            if (match instanceof RegExp)
                return match.test(s);
            else
                return !!match.find(function (m) { return m.test(s); });
        };
        return this._lazyConnect().then(function (client) {
            return new Promise(function (resolve, reject) {
                var timeout;
                var buff = '';
                var onData = function (data) {
                    var lines = data.split(exports.TELNET_EOL);
                    for (var _i = 0, lines_1 = lines; _i < lines_1.length; _i++) {
                        var line = lines_1[_i];
                        if (isMatch(line)) {
                            _this._logger && _this._logger.debug('Data read');
                            resolve([line, buff]);
                            client.removeListener('data', onData);
                            if (timeout)
                                clearTimeout(timeout);
                            break;
                        }
                        else {
                            buff = buff + lineBreak + line;
                        }
                    }
                };
                client.on('data', onData);
                if (_this._timeout) {
                    timeout = setTimeout(function () {
                        _this._logger && _this._logger.debug('Command timout');
                        reject(new Error('Did not receive matching data in time.'));
                    }, _this._timeout);
                }
            });
        });
    };
    Teletype.prototype.readUntilEnd = function (match, pageSeparator, moreCmd, lineBreak) {
        var _this = this;
        if (moreCmd === void 0) { moreCmd = '\r'; }
        if (lineBreak === void 0) { lineBreak = exports.TELNET_EOL; }
        if (!(match instanceof RegExp) && !Array.isArray(match)) {
            return Promise.reject(new TypeError('Match must be a RegExp or an array of RegExp.'));
        }
        if (!(pageSeparator instanceof RegExp)) {
            return Promise.reject(new TypeError('Page separator must be a RegExp.'));
        }
        var isMatch = function (s) {
            if (match instanceof RegExp)
                return match.test(s);
            else
                return !!match.filter(function (m) { return m.test(s); });
        };
        return this._lazyConnect().then(function (client) {
            return new Promise(function (resolve, reject) {
                var timeout;
                var buff = '';
                var onData = function (data) {
                    var lines = data.split(exports.TELNET_EOL);
                    for (var _i = 0, lines_2 = lines; _i < lines_2.length; _i++) {
                        var line = lines_2[_i];
                        if (isMatch(line)) {
                            _this._logger && _this._logger.debug('Command response completed');
                            resolve([line, buff]);
                            client.removeListener('data', onData);
                            if (timeout)
                                clearTimeout(timeout);
                            break;
                        }
                        else if (pageSeparator.test(line)) {
                            _this._logger && _this._logger.debug('Waiting for more...');
                            if (timeout)
                                clearTimeout(timeout);
                            client.write(moreCmd + exports.TELNET_EOL);
                            _setTimeout();
                        }
                        else {
                            buff = buff + lineBreak + line;
                        }
                    }
                };
                var _setTimeout = function () {
                    if (_this._timeout) {
                        timeout = setTimeout(function () {
                            _this._logger && _this._logger.debug('Command timed out');
                            reject(new Error('Did not receive matching data in time.'));
                        }, _this._timeout);
                    }
                };
                client.on('data', onData);
                _setTimeout();
            });
        });
    };
    Teletype.prototype.close = function () {
        var _this = this;
        return this._lazyConnect().then(function (client) {
            _this._logger && _this._logger.debug('Closing connection');
            client.end();
            client.destroy();
        });
    };
    return Teletype;
}());
exports.Teletype = Teletype;
//# sourceMappingURL=Teletype.js.map