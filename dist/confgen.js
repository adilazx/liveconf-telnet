"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Config_1 = require("./lib/models/Config");
var LiveCpe_1 = require("./lib/models/LiveCpe");
var logger_1 = require("./logger");
var config = require('../confgen.json');
var DB_CHECK_INTERVAL = config.interval * 60000;
var DB_FETCH_BACK = config.daysToFetchBack * 1000 * 60 * 3600;
var logger = logger_1.appLogger.child({ mod: 'CONFGEN Import' });
var lastCreated = (new Date()).setHours(-1 * DB_FETCH_BACK);
var timer;
var ipExtractor = new Map();
ipExtractor.set('CISCO_GENERIC', /(?:interface Loopback100\s?\n[\w\s]{1,}ip address )(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(?:\s255.)/i);
ipExtractor.set('M800', /(?:interface GigabitEthernet \d\/\d.\d{1,})(?:\s\n[\w\s]?)(?:ip\saddress\s)(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(?:\s\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(?:[\n\w\s"]{1,})(?:mng)/i);
ipExtractor.set('RAISECOM_GENERIC', /(?:interface\sip\s\d{1,})(?:\ndescription\s)(?:[[a-zA-Z_]{1,})?(?:mng)(?:[[a-zA-Z_]{1,})(?:\n)(?:ip\saddress\s)(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(?:\s\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})/i);
ipExtractor.set('MP1XX', /(LocalOAMIPAddress(?:\s)?=(?:\s)?)(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})/i);
ipExtractor.set('HUAWEI_GENERIC', /(?:interface\sloopback(?:\d)+)(?:[a-zA-Z\s-_\n])+(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(?:\s\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})/i);
exports.typeExtractor = new Map();
exports.typeExtractor.set('CISCO_GENERIC', /cisco/i);
exports.typeExtractor.set('M800', /mediant\s800/i);
exports.typeExtractor.set('RAISECOM_GENERIC', /raisecom/i);
exports.typeExtractor.set('MP1XX', /mp-\d\d\d/i);
exports.typeExtractor.set('HUAWEI_GENERIC', /huawei/i);
function extractMngIpAddress(type, confgenConfig) {
    if (typeof type !== 'string' || !type)
        throw new TypeError('type must be a string');
    if (typeof confgenConfig !== 'string' || !confgenConfig)
        throw new TypeError('Confgen configuration must be a string');
    if (!ipExtractor.has(type))
        throw new Error("No IP address extractor defined for type " + type);
    var regexp = ipExtractor.get(type);
    var match = regexp.exec(confgenConfig);
    if (!match)
        throw new Error("Could not extract IP address for type " + type);
    else
        return match[1];
}
exports.extractMngIpAddress = extractMngIpAddress;
function getEqTypeFromString(type) {
    if (!type)
        throw new TypeError('type must be a string');
    var cpeType;
    exports.typeExtractor.forEach(function (r, key) {
        if (r.test(type))
            cpeType = key;
    });
    if (!cpeType)
        throw new Error("No CPE type defined for " + type);
    return cpeType;
}
exports.getEqTypeFromString = getEqTypeFromString;
function findLiveConfigsByIpAddr(ipAddr, eqName) {
    return new Promise(function (resolve, reject) {
        LiveCpe_1.LiveCpeModel
            .find()
            .where('ipAddress')
            .equals(ipAddr)
            .where('eqName')
            .ne(eqName)
            .sort({ created: -1 })
            .exec(function (err, docs) {
            if (err)
                reject(err);
            else
                resolve(docs);
        });
    });
}
exports.findLiveConfigsByIpAddr = findLiveConfigsByIpAddr;
function findLiveConfigsByEqName(eqName) {
    return new Promise(function (resolve, reject) {
        LiveCpe_1.LiveCpeModel
            .find()
            .where('eqName')
            .equals(eqName)
            .sort({ created: -1 })
            .exec(function (err, docs) {
            if (err)
                reject(err);
            else
                resolve(docs);
        });
    });
}
exports.findLiveConfigsByEqName = findLiveConfigsByEqName;
function scheduleImport() {
    clearTimeout(timer);
    logger.debug("Scheduling next fetch in " + config.interval + " minutes");
    setTimeout(onTimer, DB_CHECK_INTERVAL);
}
function onTimer() {
    logger.info("Polling DB for configurations newer than " + new Date(lastCreated));
    Config_1.ConfigModel
        .find()
        .where('created')
        .gt(lastCreated)
        .sort({ created: -1 })
        .exec(function (err, docs) {
        if (err) {
            logger.error(err);
        }
        else
            processConfigs(docs);
        scheduleImport();
    });
}
function processConfigs(docs) {
    logger.debug("Found " + docs.length + " configurations");
    if (docs.length > 0)
        lastCreated = docs[0].created.getTime();
    docs.forEach(function (doc) {
        try {
            var eqName = doc.eqName;
            var eqType = getEqTypeFromString(doc.eqType);
            var ipAddr = extractMngIpAddress(eqType, doc.content);
            logger.debug("Decoded CPE " + eqName + " type " + eqType + " with IP address " + ipAddr);
            findLiveConfigsByIpAddr(ipAddr, eqName)
                .then(function (liveCpes) {
                liveCpes.length > 0 && logger.debug("Found " + liveCpes.length + " existing cpes with same IP address");
                liveCpes.forEach(function (liveCpe) { return liveCpe.update({ active: false }); });
            })
                .catch(function (err) {
                logger.warn(err);
            });
        }
        catch (err) {
            logger.warn(err);
        }
    });
}
function startConfgenImport() {
    logger.info("Importing new configurations every " + DB_CHECK_INTERVAL + " seconds.");
    onTimer();
}
exports.startConfgenImport = startConfgenImport;
function stopConfgenImport() {
    logger.info("Stopping import");
    clearTimeout(timer);
}
exports.stopConfgenImport = stopConfgenImport;
//# sourceMappingURL=confgen.js.map