"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = require("./logger");
var Mongoose = require("mongoose");
var options = require('../mongo.json');
var logger = logger_1.appLogger.child({ mod: 'mongo' });
exports.db = Mongoose.connection;
function connect(callback) {
    logger.info('Connecting to DB');
    Mongoose.connect("mongodb://" + options.host + "/" + options.dbName);
    exports.db.on('error', function (err) {
        logger.fatal(err);
        callback(err, null);
    });
    exports.db.once('open', function () {
        logger.info('Connected');
        callback(null, exports.db);
    });
}
exports.connect = connect;
//# sourceMappingURL=mongo.js.map