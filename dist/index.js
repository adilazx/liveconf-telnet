"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("babel-polyfill");
var mongo_1 = require("./mongo");
var confgen_1 = require("./confgen");
var fetcher_1 = require("./fetcher");
mongo_1.connect(function (err, connection) {
    if (!err) {
        confgen_1.startConfgenImport();
        fetcher_1.startFetch();
    }
});
//# sourceMappingURL=index.js.map