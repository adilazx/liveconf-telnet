import { LiveCpeModel, ILiveCpe, ILiveCpeModel } from './lib/models/LiveCpe';
import { FetchQueue, IFetchQueueOptions, IQueueEntry } from './lib/FetchQueue';
import { getEqTypeFromString } from './confgen';
import { appLogger } from './logger';
import * as Mongoose from 'mongoose';

const config = require('../fetcher.json') as Partial<IFetchQueueOptions>;
const logger = appLogger.child({ mod: 'FetchQueue' })


function fetchCpes(count: number): Promise<ILiveCpeModel[]> {
    return new Promise<ILiveCpeModel[]>((resolve, reject) => {
        LiveCpeModel
            .find()
            .where('isActive')
            .equals(true)
            .sort({ lastPoll: -1 })
            .limit(count)
            .exec((err, docs) => {
                if (err) reject(err);
                else resolve(docs);
            })
    })
}

function convertToQueueEntry(cpes: ILiveCpeModel[]): IQueueEntry[] {
    return cpes
        .filter(cpe => {
            try {
                getEqTypeFromString(cpe.eqType);
                if (!cpe.ipAddress) return false;
                return true;
            }
            catch (err) {
                return false;
            }
        })
        .map(cpe => {
            const cpeType = getEqTypeFromString(cpe.eqType);

            return {
                cpeId: cpe._id,
                cpeType,
                host: cpe.ipAddress
            }
        })
}

function updateCpeLastPoll(_id: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        LiveCpeModel.update(
            {
                _id,
            },
            {
                $set: {
                    lastPoll: new Date()
                }
            },
            {
                multi: false
            },
            (err, data) => {
                if (err) reject(err);
                else resolve(data);
            }
        )
    })
}


let options: IFetchQueueOptions = {
    maxConcurrentJobs: 2,
    startHour: 0,
    endHour: 23,
    logger,
    onJobStart: (job) => {
        // update last poll for jpb/cpe
        updateCpeLastPoll(job.cpeId).catch(err => logger.warn(err));
    },
    onJobDone: (job, err, config) => {
        // feed queue
        fetchCpes(1)
            .then(cpes => {
                queue.addJobs(...convertToQueueEntry(cpes));
            });

        console.log(job.cpeId, err ? err.message : null, )
    },
    onQueueDone: () => {

    }
}

options = Object.assign(options, config);

const queue = new FetchQueue(options);

export function startFetch() {

    fetchCpes(options.maxConcurrentJobs)
        .then(cpes => {
            queue.addJobs(...convertToQueueEntry(cpes));
            queue.start();
        })
        .catch(err => logger.error(err));
}