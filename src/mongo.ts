import { appLogger } from './logger';
import * as Mongoose from 'mongoose';
const options = require('../mongo.json') as IMongooseOptions;

export interface IMongooseOptions {
    readonly host: string;
    readonly dbName: string;
}
const logger = appLogger.child({ mod: 'mongo' });
export const db = Mongoose.connection;
export function connect(callback: (err: Error, connection: Mongoose.Connection) => void) {
    logger.info('Connecting to DB');
    Mongoose.connect(`mongodb://${options.host}/${options.dbName}`);

    db.on('error', (err: Error) => {
        logger.fatal(err);
        callback(err, null);
    });

    db.once('open', () => {
        logger.info('Connected');
        callback(null, db);
    });
}