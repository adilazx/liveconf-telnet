import * as Bunyan from 'bunyan';
const options = require('../logger.json')

export const appLogger = Bunyan.createLogger(options);