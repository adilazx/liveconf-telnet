require("babel-polyfill");
import { db, connect } from './mongo';
import { startConfgenImport, stopConfgenImport } from './confgen';
import { startFetch } from './fetcher';

connect((err, connection) => {
    if (!err) {
        startConfgenImport();
        startFetch();
    }
});

/*
import { appLogger } from './logger';
import { Teletype } from './lib/Teletype';
import { connect } from './lib/Connect';
import { drivers, TConfigFetcher } from './lib/drivers';
import { FetchQueue } from './lib/FetchQueue';
const options = require('../options.json');

const queue = new FetchQueue({
    startHour: 11,
    endHour: 23,
    logger: appLogger.child({ mod: 'FetchQueue' }),
    maxConcurrentJobs: 2,
    onJobDone: (job, err, config) => {
        console.log(job.cpeId, '---------------------------------------------------------------------------------------')
        //if (err) console.log(err);
        //else console.log(config)
        //console.log('--------------------------------------------------------------------------------------------------');
    },
    onQueueDone: () => {
        queue.stop();
    }
})

queue.addJobs(
    {
        cpeId: '1',
        cpeType: 'M800',
        host: '10.255.48.10'
    },

    {n
        cpeId: '2',
        cpeType: 'MP1XX',
        host: '10.255.191.158'
    }
);

queue.start();
*/

/*

console.log(options)


const testM800 = () => {
    return drivers.get('M800').fetchConfig('10.255.48.10', options['M800']);
}

testM800()
    .then(config => {
        console.log('Done 1');
        return testM800();
    })
    .then(config => {
        console.log('Done all');
    })
    .catch(err => console.log(err));

*/

/*
drivers.get('MP1XX')
    .fetchConfig('10.255.191.158', options['MP1XX'])
    .then(config => console.log(config))
    .catch(err => console.log(err));
*/


/*
//M800 example
connect('10.255.48.10', {
    loginPrompt: /username:/i,
    passwordPrompt: /password:/i,
    shellPrompt: /([a-zA-Z0-9_]{1,})(\#|\>)/,
    credentials: [
        ['aaa', 'bbb'],
        ['icg', 'icg1@#']
    ],
    timeout: 5000,
    logger: appLogger
})
    .then(connection => {
        console.log('done', connection.prompt);
    })
    .catch(err => {
        console.log(err)
    })

    */

// MP-1XX example
/*
connect('10.255.191.158', {
    loginPrompt: /login:/i,
    passwordPrompt: /password:/i,
    shellPrompt: /\/>/,
    credentials: [
        ['icg', 'icg1@#']
    ],
    timeout: 5000,
    logger: appLogger
})
    .then(connection => {
        console.log('done', connection.prompt);
    })
    .catch(err => {
        console.log(err)
    })
*/

/*
const client = new Teletype('10.255.48.10', {
    timeout: 5000,
    logger: appLogger.child({ host: '10.255.48.10' })
});

let prompt: string;

client.readUntil(/username:/i)
    .then((response) => {
        return client.exec('icg', /password:/i);
    })
    .then((response) => {
        return client.exec('icg1@#', /([a-zA-Z0-9_]{1,})(\#|\>)/i);
    })
    .then((response) => {
        prompt = response[0];
        console.log(prompt)
        return client.exec('show running-config', /\w/);
    })
    .then((response) => {
        return client.readUntilEnd(new RegExp(prompt), /--MORE--/, '   ');
    })
    .then((response) => {
        console.log(response[1]);
        client.close();
    })
    .catch((err: Error) => {
        console.log(err);
        client.close();        
    })
*/