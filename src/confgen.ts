import { ConfigModel, IConfig, IConfigModel } from './lib/models/Config';
import { LiveCpeModel, ILiveCpe, ILiveCpeModel } from './lib/models/LiveCpe';
import * as Mongoose from 'mongoose';
import { appLogger } from './logger';
import { TCpeType } from './lib';
const config = require('../confgen.json');

const DB_CHECK_INTERVAL = config.interval * 60000;
const DB_FETCH_BACK = config.daysToFetchBack * 1000 * 60 * 3600;


const logger = appLogger.child({ mod: 'CONFGEN Import' });
let lastCreated: number = (new Date()).setHours(-1 * DB_FETCH_BACK);
let timer: NodeJS.Timer;

const ipExtractor: Map<TCpeType, RegExp> = new Map();
ipExtractor.set('CISCO_GENERIC', /(?:interface Loopback100\s?\n[\w\s]{1,}ip address )(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(?:\s255.)/i);
ipExtractor.set('M800', /(?:interface GigabitEthernet \d\/\d.\d{1,})(?:\s\n[\w\s]?)(?:ip\saddress\s)(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(?:\s\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(?:[\n\w\s"]{1,})(?:mng)/i);
ipExtractor.set('RAISECOM_GENERIC', /(?:interface\sip\s\d{1,})(?:\ndescription\s)(?:[[a-zA-Z_]{1,})?(?:mng)(?:[[a-zA-Z_]{1,})(?:\n)(?:ip\saddress\s)(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(?:\s\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})/i);
ipExtractor.set('MP1XX', /(LocalOAMIPAddress(?:\s)?=(?:\s)?)(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})/i);
ipExtractor.set('HUAWEI_GENERIC', /(?:interface\sloopback(?:\d)+)(?:[a-zA-Z\s-_\n])+(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(?:\s\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})/i);

export const typeExtractor: Map<TCpeType, RegExp> = new Map();
typeExtractor.set('CISCO_GENERIC', /cisco/i);
typeExtractor.set('M800', /mediant\s800/i);
typeExtractor.set('RAISECOM_GENERIC', /raisecom/i);
typeExtractor.set('MP1XX', /mp-\d\d\d/i);
typeExtractor.set('HUAWEI_GENERIC', /huawei/i);

export function extractMngIpAddress(type: TCpeType, confgenConfig: string): string {
    if (typeof type !== 'string' || !type) throw new TypeError('type must be a string');
    if (typeof confgenConfig !== 'string' || !confgenConfig) throw new TypeError('Confgen configuration must be a string');
    if (!ipExtractor.has(type)) throw new Error(`No IP address extractor defined for type ${type}`);
    const regexp = ipExtractor.get(type as TCpeType);
    const match = regexp.exec(confgenConfig);
    if (!match) throw new Error(`Could not extract IP address for type ${type}`);
    else return match[1];
}

export function getEqTypeFromString(type: string): TCpeType {
    if (!type) throw new TypeError('type must be a string');
    let cpeType: TCpeType;
    typeExtractor.forEach((r, key) => {
        if (r.test(type)) cpeType = key;
    });
    if (!cpeType) throw new Error(`No CPE type defined for ${type}`);
    return cpeType;
}

export function findLiveConfigsByIpAddr(ipAddr: string, eqName: string): Promise<ILiveCpeModel[]> {
    return new Promise<ILiveCpeModel[]>((resolve, reject) => {
        LiveCpeModel
            .find()
            .where('ipAddress')
            .equals(ipAddr)
            .where('eqName')
            .ne(eqName)
            .sort({ created: -1 })
            .exec((err, docs) => {
                if (err) reject(err);
                else resolve(docs);
            })
    })
}

export function findLiveConfigsByEqName(eqName: string): Promise<ILiveCpeModel[]> {
    return new Promise<ILiveCpeModel[]>((resolve, reject) => {
        LiveCpeModel
            .find()
            .where('eqName')
            .equals(eqName)
            .sort({ created: -1 })
            .exec((err, docs) => {
                if (err) reject(err);
                else resolve(docs);
            })
    })
}

function scheduleImport() {
    clearTimeout(timer);
    logger.debug(`Scheduling next fetch in ${config.interval} minutes`);
    setTimeout(onTimer, DB_CHECK_INTERVAL);
}

function onTimer() {
    logger.info(`Polling DB for configurations newer than ${new Date(lastCreated)}`);

    ConfigModel
        .find()
        .where('created')
        .gt(lastCreated)
        .sort({ created: -1 })
        .exec((err, docs) => {
            if (err) {
                logger.error(err);
            }
            else processConfigs(docs);
            scheduleImport();
        });
}

function processConfigs(docs: IConfigModel[]) {
    logger.debug(`Found ${docs.length} configurations`);
    if (docs.length > 0) lastCreated = docs[0].created.getTime();
    docs.forEach(doc => {
        try {
            const eqName = doc.eqName;
            const eqType = getEqTypeFromString(doc.eqType);
            const ipAddr = extractMngIpAddress(eqType, doc.content);
            logger.debug(`Decoded CPE ${eqName} type ${eqType} with IP address ${ipAddr}`);
            findLiveConfigsByIpAddr(ipAddr, eqName)
                .then(liveCpes => {
                    liveCpes.length > 0 && logger.debug(`Found ${liveCpes.length} existing cpes with same IP address`);
                    liveCpes.forEach(liveCpe => liveCpe.update({ active: false }));
                })
                .catch(err => {
                    logger.warn(err);
                })
        }
        catch (err) {
            logger.warn(err);
        }
    })
}

export function startConfgenImport() {
    logger.info(`Importing new configurations every ${DB_CHECK_INTERVAL} seconds.`);
    onTimer();
}

export function stopConfgenImport() {
    logger.info(`Stopping import`);
    clearTimeout(timer);
}
