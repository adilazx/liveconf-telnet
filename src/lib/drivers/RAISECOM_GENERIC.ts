import { connect, enable, TConfigFetcher, IConnectOptions, IConnectRes, IEnableOptions } from './';
import { appLogger } from '../../logger';
const credentials = require('../../../credentials.json');
const type = 'RAISECOM_GENERIC';

const defaultOptions: IConnectOptions = {
    loginPrompt: /login:/i,
    passwordPrompt: /password:/i,
    shellPrompt: /([a-zA-Z0-9_-]{1,})(\#|\>)/,
    failedLoginMatch: /login/i,
    credentials: credentials[type],
    sendOnConnect: '\r',
    timeout: 5000,
    logger: appLogger.child({ type })
}

const fetcher: TConfigFetcher = (host: string, options: Partial<IConnectOptions & IEnableOptions> = {}) => {

    return new Promise<string>((resolve, reject) => {

        connect(host, Object.assign(defaultOptions, options))
            .then(connection => {
                enable(connection, {
                    enablePrompt: options.passwordPrompt,
                    enableShellPrompt: new RegExp(connection.prompt.substr(0, connection.prompt.length - 2)),
                    logger: options.logger,
                    enablePassword: options.enablePassword
                })
                    .then(data => connection.teletype.exec('show running-config', /system/))
                    .then(data => {
                        return connection.teletype.readUntilEnd(new RegExp(connection.prompt), /--More--/i, ' ');
                    })
                    .then(data => {
                        connection.teletype.exec('exit', null);
                        connection.teletype.close();
                        resolve(data[1])
                    })
                    .catch(err => {
                        connection.teletype.close();
                        reject(err);
                    });
            })
            .catch(err => {
                reject(err);
            });
    })
}

export default fetcher;