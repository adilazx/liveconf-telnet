import { connect, TConfigFetcher, IConnectOptions, IConnectRes } from './';
import { appLogger } from '../../logger';
const credentials = require('../../../credentials.json');
const type = 'MP1XX';


const defaultOptions: IConnectOptions = {
    loginPrompt: /login:/i,
    passwordPrompt: /password:/i,
    shellPrompt: /\/>/,
    failedLoginMatch: /denied/i,
    credentials: credentials[type],
    timeout: 5000,
    logger: appLogger.child({ type })
}

const fetcher: TConfigFetcher = (host: string, options: Partial<IConnectOptions> = {}) => {

    return new Promise<string>((resolve, reject) => {

        connect(host, Object.assign(defaultOptions, options))
            .then(connection => {
                connection
                    .teletype.exec('sh log stop', /\/>/)
                    .then(data => connection.teletype.exec('conf', /\/CONFiguration>/i))
                    .then(data => {
                        connection.teletype.exec('CF VIEW', /\*/);
                        return connection.teletype.readUntilEnd(/CopyConfigurationFile/i, /-- More --/i, '\r\n');
                    })
                    .then(data => {
                        connection.teletype.exec('exit', null);
                        connection.teletype.close();
                        resolve(data[1])
                    })
                    .catch(err => {
                        reject(err);
                        connection.teletype.close();
                    })
            })
            .catch(err => {
                reject(err);
            });
    })
}

export default fetcher;