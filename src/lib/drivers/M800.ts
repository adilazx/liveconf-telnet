import { connect, TConfigFetcher, IConnectOptions, IConnectRes } from './';
import { appLogger } from '../../logger';
const credentials = require('../../../credentials.json');
const type = 'M800';


const defaultOptions: IConnectOptions = {
    loginPrompt: /username:/i,
    passwordPrompt: /password:/i,
    shellPrompt: /([a-zA-Z0-9_-]{1,})(\#|\>)/,
    failedLoginMatch: /denied/i,
    credentials: credentials[type],
    timeout: 5000,
    logger: appLogger.child({ type })
}

const fetcher: TConfigFetcher = (host: string, options: Partial<IConnectOptions> = {}) => {

    return new Promise<string>((resolve, reject) => {

        connect(host, Object.assign(defaultOptions, options))
            .then(connection => {
                connection
                    .teletype.exec('show running-config', /\w/)
                    .then(data => {
                        return connection.teletype.readUntilEnd(new RegExp(connection.prompt), /--MORE--/i, '   ');
                    })
                    .then(data => {
                        connection.teletype.exec('exit', null);
                        connection.teletype.close();
                        resolve(data[1])
                    })
                    .catch(err => {
                        connection.teletype.close();
                        reject(err);
                    });
            })
            .catch(err => {
                reject(err);
            });
    })
}

export default fetcher;