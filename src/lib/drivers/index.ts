import { IConnectOptions, TCredentialTuple } from '../Connect';
export { IConnectOptions, connect, IConnectRes, enable, IEnableOptions } from '../Connect';
import { Teletype } from '../Teletype';
import { TCpeType } from '../';
import M800 from './M800';
import MP1XX from './MP1XX';
import CISCO_GENERIC from './CISCO_GENERIC';
import RAISECOM_GENERIC from './RAISECOM_GENERIC';
import HUAWEI_GENERIC from './HUAWEI_GENERIC';


export type TConfigFetcher = (host: string, options?: Partial<IConnectOptions>) => Promise<string>;

export const drivers: Map<TCpeType, { fetchConfig: TConfigFetcher }> = new Map();

drivers.set('M800', { fetchConfig: M800 });
drivers.set('MP1XX', { fetchConfig: MP1XX });
drivers.set('CISCO_GENERIC', { fetchConfig: CISCO_GENERIC });
drivers.set('RAISECOM_GENERIC', { fetchConfig: RAISECOM_GENERIC });
drivers.set('HUAWEI_GENERIC', { fetchConfig: HUAWEI_GENERIC });
