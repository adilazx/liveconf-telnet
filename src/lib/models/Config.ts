import * as Mongoose from 'mongoose';

export interface IConfig {
    csc: string;
    clientName: string;
    eqType: string;
    eqName: string;
    wo: string;
    content: string;
    isActive: boolean;
    created: Date;
    createdBy: Date;   
}

export interface IConfigModel extends IConfig, Mongoose.Document {

}

export const ConfigSchema = new Mongoose.Schema({
    csc: {type: String, required: true, index: {unique: false}},
    clientName: {type: String, required: false},
    eqType: {type: String, required: true},
    eqName: {type: String, required: true},
    wo: {type: String, required: false},
    content: {type: String, required: true},
    isActive: {type: Boolean, default: true},
    isExported: {type: Boolean},
    fileName: {type: String},
    mac: {type: String},
    version: {type: Number, default: 1, index: {unique: false}},
    created: {type: Date},
    createdBy: {type: String},
    exported: {type: Date},
    exportedBy: {type: String},
    downloaded: {type: Date},
    dlMethod: {type: String},
    dlLink: {type: String}
});

export const ConfigModel = Mongoose.model<IConfigModel>('Config', ConfigSchema);