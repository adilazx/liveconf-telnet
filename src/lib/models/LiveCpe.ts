import * as Mongoose from 'mongoose';

export interface ILiveCpe {
    csc: string;
    clientName: string;
    eqType: string;
    eqName: string;
    wo: string;
    isActive: boolean;
    isCeased: boolean;
    ipAddress: string;
    created: Date;
    modified: Date;
    failures: number;
    lastError: string;
    lastPoll: Date;
    lastFetch: Date;
    nextPoll: Date;
    configsCount: number;
    pollsCount: number;
    hash: string;
    configs: { output: string; created: Date }[];
}

export interface ILiveCpeModel extends ILiveCpe, Mongoose.Document {

}

export const LiveCpeSchema = new Mongoose.Schema({
    csc: { type: String, required: true, index: { unique: false } },
    clientName: { type: String, required: false },
    eqType: { type: String, required: true },
    eqName: { type: String, required: true },
    wo: { type: String, required: false },
    isActive: { type: Boolean, default: true },
    isCeased: { type: Boolean, default: false },
    ipAddress: { type: String, required: true },
    created: { type: Date, default: Date.now },
    modified: { type: Date },
    failures: { type: Number, default: 0 },
    lastError: { type: String },
    lastPoll: { type: Date },
    lastFetch: { type: Date },
    nextPoll: { type: Date },
    configsCount: { type: Number, default: 0 },
    pollsCount: { type: Number, default: 0 },
    hash: { type: String },
    configs: [{ output: String, created: Date }],
});

export const LiveCpeModel = Mongoose.model<ILiveCpeModel>('LiveCpe', LiveCpeSchema);