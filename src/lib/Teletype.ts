import * as net from 'net';
import * as Bunyan from 'bunyan';

// “The Telnet protocol defines the sequence CR LF to mean "end-of-line".”
// See https://tools.ietf.org/html/rfc1123#page-21.
export const TELNET_EOL = '\r\n';

export interface ITeletypeOptions {
    port?: number;
    timeout?: false | number;
    logger?: Bunyan;
}


/**
 * Teletype class
 */
export class Teletype {

    protected _host: string;
    protected _port: number;
    protected _timeout: number | false;
    protected _client: net.Socket;
    protected _logger: Bunyan;

    constructor(host: string, options: ITeletypeOptions = {}) {

        this._host = host;
        this._port = options.port || 23;
        this._timeout = options.timeout || options.timeout === false ? options.timeout : false;
        this._logger = options.logger;

        if (typeof host !== 'string') {
            throw new TypeError('Host must be a string.');
        }
        if (typeof this._port !== 'number') {
            throw new TypeError('port must be a number.');
        }
        if (typeof this._timeout !== 'number' && this._timeout !== false) {
            throw new TypeError('options.timeout must be a number or false.');
        }
    }

    public get logger() {
        return this._logger;
    }

    public get host() {
        return this._host;
    }

    public get port() {
        return this._port;
    }

    protected _lazyConnect(): Promise<net.Socket> {
        return new Promise<net.Socket>((resolve, reject) => {
            let timeout: NodeJS.Timer;

            if (this._client && !(this._client as any).connecting) {
                return resolve(this._client)
            }

            if (!this._client) {
                this._logger && this._logger.debug('Connecting...');
                this._client = net.connect({
                    host: this._host,
                    port: this._port
                })

                if (this._timeout) {
                    timeout = setTimeout(() => {
                        this._logger && this._logger.debug('Connection timeout');
                        this._client.destroy()
                        reject(new Error('Connect timeout'));
                    }, this._timeout)
                }

                // “The TELNET protocol is based upon the notion of a virtual teletype,
                // employing a 7-bit ASCII character set.”
                // See https://tools.ietf.org/html/rfc206#page-2.
                this._client.setEncoding('ascii');

                this._client.once('error', (err) => {
                    this._logger && this._logger.error(err);
                    if (timeout) clearTimeout(timeout);
                    reject(err);
                });

                this._client.once('connect', () => {
                    this._logger && this._logger.debug('Connected');
                    this._client.removeListener('error', reject);
                    if (timeout) clearTimeout(timeout);
                    resolve(this._client);
                });

                this._client.once('end', () => {
                    this._logger && this._logger.debug('Socket disconnected by remote end');
                });

                this._client.once('close', () => {
                    this._logger && this._logger.debug('Socket closed');
                });                
            }
        })
    }

    public exec(command: string, match: RegExp | RegExp[], lineBreak: string = TELNET_EOL): Promise<[string, string]> {
        if (typeof command !== 'string') {
            return Promise.reject(new TypeError('Command must be a string.'));
        }

        this._logger && this._logger.debug(`Executing [${command}]`);

        return this._lazyConnect().then(client => {
            let promise;

            if (match) {
                promise = this.readUntil(match, lineBreak);
            }

            client.write(command + TELNET_EOL);

            if (match) return promise;
        })
    }

    public readUntil(match: RegExp | RegExp[], lineBreak: string = TELNET_EOL): Promise<[string, string]> {
        if (!(match instanceof RegExp) && !Array.isArray(match)) {
            return Promise.reject(new TypeError('Match must be a RegExp or an array of RegExp.'));
        }

        const isMatch = (s: string): boolean => {
            if (match instanceof RegExp) return match.test(s);
            else return !!match.find(m => m.test(s));
        }

        return this._lazyConnect().then(client => {
            return new Promise((resolve, reject) => {
                let timeout: NodeJS.Timer;
                let buff: string = '';

                const onData = (data: string) => {
                    const lines = data.split(TELNET_EOL);

                    for (const line of lines) {
                        if (isMatch(line)) {
                            this._logger && this._logger.debug('Data read');
                            resolve([line, buff]);
                            client.removeListener('data', onData);
                            if (timeout) clearTimeout(timeout);
                            break;
                        }
                        else {
                            buff = buff + lineBreak + line;
                        }
                    }
                }

                client.on('data', onData);

                if (this._timeout) {
                    timeout = setTimeout(() => {
                        this._logger && this._logger.debug('Command timout');
                        reject(new Error('Did not receive matching data in time.'));
                    }, this._timeout);
                }
            });
        })
    }

    public readUntilEnd(match: RegExp | RegExp[], pageSeparator: RegExp, moreCmd: string = '\r', lineBreak: string = TELNET_EOL): Promise<[string, string]> {
        if (!(match instanceof RegExp) && !Array.isArray(match)) {
            return Promise.reject(new TypeError('Match must be a RegExp or an array of RegExp.'));
        }
        if (!(pageSeparator instanceof RegExp)) {
            return Promise.reject(new TypeError('Page separator must be a RegExp.'));
        }

        const isMatch = (s: string): boolean => {
            if (match instanceof RegExp) return match.test(s);
            else return !!match.filter(m => m.test(s));
        }

        return this._lazyConnect().then(client => {
            return new Promise((resolve, reject) => {
                let timeout: NodeJS.Timer;
                let buff: string = '';

                const onData = (data: string) => {
                    const lines = data.split(TELNET_EOL);

                    for (const line of lines) {
                        if (isMatch(line)) {
                            this._logger && this._logger.debug('Command response completed');
                            resolve([line, buff]);
                            client.removeListener('data', onData);
                            if (timeout) clearTimeout(timeout);
                            break;
                        }
                        else if (pageSeparator.test(line)) {
                            this._logger && this._logger.debug('Waiting for more...');
                            if (timeout) clearTimeout(timeout);
                            client.write(moreCmd + TELNET_EOL);
                            _setTimeout();
                        }
                        else {
                            buff = buff + lineBreak + line;
                        }
                    }
                }

                const _setTimeout = () => {
                    if (this._timeout) {
                        timeout = setTimeout(() => {
                            this._logger && this._logger.debug('Command timed out');
                            reject(new Error('Did not receive matching data in time.'));
                        }, this._timeout);
                    }
                }

                client.on('data', onData);
                _setTimeout();
            });
        })
    }

    public close() {
        return this._lazyConnect().then(client => {
            this._logger && this._logger.debug('Closing connection');
            client.end();
            client.destroy();
        })
    }
}