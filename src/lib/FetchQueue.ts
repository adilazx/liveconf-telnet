import * as Bunyan from 'bunyan';
import { drivers } from './drivers';
import { TCpeType } from './';
const options = require('../../options.json');

// -

export interface IQueueEntry {
    readonly cpeId: string;
    readonly cpeType: TCpeType;
    readonly host: string;
    active?: boolean;
}

export interface IFetchQueueOptions {
    maxConcurrentJobs: number;
    logger: Bunyan;
    startHour: number;
    endHour: number;
    readonly onJobStart: (job: IQueueEntry) => void;
    readonly onJobDone: (job: IQueueEntry, error: Error, config: string) => void;
    readonly onQueueDone: () => void;
}

/**
 * FetchQueue
 */
export class FetchQueue {

    protected maxConcurrentJobs: number;
    protected logger: Bunyan;
    protected onJobStart: (job: IQueueEntry) => void;
    protected onJobDone: (job: IQueueEntry, error: Error, config: string) => void;
    protected onQueueDone: () => void;
    protected startHour: number;
    protected endHour: number;
    protected queue: IQueueEntry[] = [];
    protected _running: boolean = false;
    protected _processingQueue: boolean = false;
    protected timer: NodeJS.Timer;

    constructor(options: IFetchQueueOptions) {
        this.maxConcurrentJobs = options.maxConcurrentJobs;
        this.logger = options.logger;
        this.onJobStart = options.onJobStart;
        this.onJobDone = options.onJobDone;
        this.onQueueDone = options.onQueueDone;
        this.startHour = options.startHour;
        this.endHour = options.endHour;
    }

    public addJobs(...entries: IQueueEntry[]) {
        this.queue.push(...entries);
        this.logger.debug(`Added ${entries.length} jobs`);
    }

    public get size() {
        return this.queue.length;
    }

    public get running() {
        return this._running;
    }

    public start() {
        if (!this._running) {
            this.timer = setInterval(this.onTimer, 5000);
            this._running = true;
            this.logger.info('Starting');
        }
    }

    public stop() {
        if (this._running) {
            clearInterval(this.timer);
            this._running = false;
            this.logger.info('Stopped');
        }
    }

    protected onTimer = () => {
        const d = new Date();
        const hours = d.getHours();
        if (this.startHour > this.endHour && (hours >= this.startHour || hours < this.endHour)) this.startProcessingQueue();
        else if (hours >= this.startHour && hours < this.endHour) this.startProcessingQueue();
        else this.stopProcessingQueue();
    }

    protected startProcessingQueue() {
        if (this._processingQueue) return;
        this.logger.info('Start processing queue');
        this._processingQueue = true;
        this.executeJobs();
    }

    protected stopProcessingQueue() {
        if (!this._processingQueue) return;
        this.logger.info('Stop processing queue');
        this._processingQueue = false;
    }

    protected onJobCompleted(job: IQueueEntry, err: Error, config: string) {
        this.logger.debug(`Job completed [${job.cpeId}]`);
        const jobIndex = this.queue.indexOf(job);
        this.queue.splice(jobIndex, 1);
        this.onJobDone(job, err, config);

        this.executeJobs();
    }

    public get activeCount() {
        return this.queue.filter(entry => entry.active === true).length;
    }

    protected executeJobs() {
        if (this.size > 0) {
            const activeJobsCount = this.activeCount;
            activeJobsCount < this.maxConcurrentJobs && this.logger.info(`Executing jobs, active jobs: ${activeJobsCount}, queue size: ${this.size}`)
            this.queue.forEach((job) => {
                if (!job.active && activeJobsCount < this.maxConcurrentJobs) {
                    this.executeJob(job);
                }
            })
        }
        else {
            this.logger.warn('Queue is empty');
            this.onQueueDone();
        }
    }

    protected executeJob(job: IQueueEntry) {
        job.active = true;
        this.logger.info(`Running job [${job.cpeId}]`);
        this.onJobStart(job);
        drivers.get(job.cpeType).fetchConfig(job.host, options[job.cpeType])
            .then(config => {
                this.onJobCompleted(job, null, config);
            })
            .catch(err => this.onJobCompleted(job, err, null));
    }
}