import * as Bunyan from 'bunyan';
import { Teletype } from './Teletype';

// --

export type TCredentialTuple = [string, string];

export interface IConnectOptions {
    port?: number;
    sendOnConnect?: string;
    credentials: TCredentialTuple[];
    loginPrompt: RegExp;
    passwordPrompt: RegExp;
    shellPrompt: RegExp;
    failedLoginMatch: RegExp;
    logger?: Bunyan;
    lineBreak?: string;
    timeout: number | false;
}

export interface IConnectRes {
    prompt: string;
    teletype: Teletype;
    credentials: TCredentialTuple;
}

/**
 * connect utility function
 */
export const connect = (host: string, options: IConnectOptions): Promise<IConnectRes> => {

    if (typeof options !== 'object' || !options) throw new TypeError("Invalid options");
    if (typeof host !== 'string') throw new TypeError('Host must be a string.');
    if (!(options.loginPrompt instanceof RegExp)) throw new TypeError('Invalid login prompt.');
    if (!(options.passwordPrompt instanceof RegExp)) throw new TypeError('Invalid password prompt.');
    if (!(options.shellPrompt instanceof RegExp)) throw new TypeError('Invalid shell prompt.');
    if (!(options.failedLoginMatch instanceof RegExp)) throw new TypeError('Invalid failed login match.');
    if (!Array.isArray(options.credentials) || options.credentials.length === 0) throw new TypeError('Invalid creadentials.');

    const { credentials, lineBreak, loginPrompt, passwordPrompt, shellPrompt, failedLoginMatch, port, timeout, sendOnConnect } = options;
    const maxAttempts = options.credentials.length;
    const logger = options.logger ? options.logger.child({ host }) : null;
    let teletype: Teletype;


    const attemptConnect = (attempt: number): Promise<IConnectRes> => {

        return new Promise<IConnectRes>((resolve, reject) => {
            teletype = new Teletype(host, {
                logger,
                port,
                timeout
            });

            const username = credentials[attempt][0];
            const password = credentials[attempt][1];

            if (sendOnConnect) teletype.exec(sendOnConnect, null);

            teletype
                .readUntil(loginPrompt, lineBreak)
                .then((res) => {
                    logger && logger.debug(`Sending username`);
                    return teletype.exec(username, passwordPrompt, lineBreak);
                })
                .then((res) => {
                    logger && logger.debug(`Sending password`);
                    return teletype.exec(password, [shellPrompt, failedLoginMatch], lineBreak);
                })
                .then(res => {
                    const prompt = res[0];
                    if (failedLoginMatch.test(prompt)) {
                        logger && logger.info(`Authentication failed`);
                        teletype.close();
                        return reject(new Error('Authentication failed'));
                    };
                    logger && logger.info(`Authenticated, prompt is [${prompt}]`);
                    resolve({
                        teletype,
                        prompt,
                        credentials: [username, password]
                    })
                })
                .catch(err => {
                    logger && logger.warn(err.message);
                    teletype.close();
                    reject(err);
                });
        });
    }


    return new Promise<IConnectRes>((resolve, reject) => {

        logger && logger.info(`Connecting to ${host}`);

        const executeAttempt = (attempt: number = 0) => {
            logger && logger.debug(`Connection attempt #${attempt}`);

            attemptConnect(attempt)
                .then(res => resolve(res))
                .catch(err => {
                    if (attempt === maxAttempts - 1) reject(new Error('All connection attempts have failed.'));
                    else executeAttempt(attempt + 1);
                });
        }

        executeAttempt();
    });
}


// --

export interface IEnableOptions {
    enablePrompt: RegExp;
    enableShellPrompt: RegExp;
    enablePassword: string;
    logger?: Bunyan;
}

/**
 * enable utility function
 */
export const enable = (connection: IConnectRes, options: IEnableOptions): Promise<string> => {

    const { enablePrompt, enableShellPrompt, enablePassword, logger } = options;

    return new Promise<string>((resolve, reject) => {

        if (!enablePassword || typeof enablePassword !== 'string') return reject(new TypeError('invalid enable password'));

        logger && logger.debug('Enabling....');

        connection.teletype.exec('enable', enablePrompt)
            .then(data => connection.teletype.exec(enablePassword, enableShellPrompt))
            .then(data => {
                const prompt = data[0];
                connection.prompt = prompt;
                logger && logger.debug(`Enabled, prompt is now ${prompt}`);
                resolve(prompt);
            })
            .catch(err => {
                logger && logger.debug(`Enable failed`);
                reject(err);
            })
    })
}